## Descrição

Esse serviço visa organizar uma lista de eventos com base no tempo de duração de cada eventos.

Optei em gerar uma Classe abstrata pensando na reutilização do código para outros tipos de eventos que não sejam "talks"
como nesse caso. Com uma classe genérica podemos reutilizar os metódos já disponíveis.

O classe Talks é responsável por gerar as listas individualmente. 

Também utilizei collections em grande parte do código de modo a deixar o código mais limpo e intuitivo.
 
### Como utilizar essa ferramenta?

Acesse o [link](http://quero-escola.herokuapp.com/forms/send-events-file/create) nesse link é possível enviar um arquivo (.TXT) contendo uma lista de eventos separados por vírgula.

### Como visualizar o resultado da API?

Acesse o [link](https://quero-escola.herokuapp.com/api/v1/meetings/talks)
