<?php

Route::group(['prefix' => 'v1'], function () {
    Route::group(['namespace' => 'Meetings', 'prefix' => 'meetings'], function () {
        Route::get('/talks', 'Talks@showList');
    });
});
