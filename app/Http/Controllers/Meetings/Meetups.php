<?php


namespace App\Http\Controllers\Meetings;


use Illuminate\Http\JsonResponse;

interface Meetups
{
    public function showList(): JsonResponse ;
}
