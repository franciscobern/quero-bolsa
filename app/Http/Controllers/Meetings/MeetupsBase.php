<?php


namespace App\Http\Controllers\Meetings;


use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

abstract class MeetupsBase
{
    public $startFirstPeriod;
    public $hourLunch;
    public $startSecondPeriod;
    public $happyHour;

    public function duration(string $value, string $textToTime = null, string $timeToReplaceText = null): string
    {
        $resultMatch = $this->getMatchTimeInString($value);
        return strpos($value, $textToTime) ? $timeToReplaceText : $this->getTimeExtractedToString($resultMatch);
    }

    public function horary(string $start, string $interval): string
    {
        return Carbon::createFromFormat('H:i', $start)
            ->addMinutes(intval($interval))->format('H:i');
    }

    public function title(string $text): string
    {
        return $text;
    }

    public function removeProcessedPeriod(Collection $collection, array $removedItems): array
    {
        $restItemsToProcess = $collection->filter(function ($value, $key) use ($removedItems) {

            $removedKey = collect($removedItems)->filter(function ($val) use ($key) {
                return $val === $key;
            })->toArray();

            return $key !== Arr::first($removedKey) ? $value : null;
        })->toArray();

        return array_values($restItemsToProcess);
    }

    protected function getTimeExtractedToString(array $array): string
    {
        return Arr::last($array);
    }

    protected function getMatchTimeInString(string $string): array
    {
        preg_match("/([0-9]+)min/", $string, $matches);
        return $matches;
    }
}
