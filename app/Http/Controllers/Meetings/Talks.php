<?php


namespace App\Http\Controllers\Meetings;


use Illuminate\Http\JsonResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;

class Talks extends MeetupsBase implements Meetups
{
    private $data;
    public $startFirstPeriod = "09:00";
    public $hourLunch = "12:00 Hour Lunch";
    public $startSecondPeriod = "01:00";
    public $happyHour = "05:00 Happy Hour";
    private $processedPeriod;
    private $restProcessedPeriod;

    public function __construct()
    {
        $this->processedPeriod = [];
        $this->restProcessedPeriod = null;
    }

    public function showList(): JsonResponse
    {
        return response()->json($this->createListTalks(), 200);
    }

    private function createListTalks(): array
    {
        $collection = $this->getTitleAndDurationOfTalk();

        $count = 1;
        while (empty($this->processedPeriod) || !empty($this->restProcessedPeriod)) {
            $results = empty($this->restProcessedPeriod) ?
                $this->processAllPeriods($collection) : $this->processAllPeriods(collect($this->restProcessedPeriod));

            $this->processedPeriod[] = [
                'title' => "Track $count",
                'data' => Arr::first($results)
            ];
            $this->restProcessedPeriod = Arr::last($results);
            $count++;
        }

        return [
            'data' => $this->processedPeriod
        ];
    }

    private function getTitleAndDurationOfTalk(): Collection
    {
        return collect($this->getDataFile())->map(function ($values) {
            return [
                'title' => $this->title($values),
                'duration' => $this->duration($values, 'lightning', '5'),
            ];
        });
    }

    private function processAllPeriods(Collection $collection)
    {
        $firstPeriod = $this->processedPeriodAndGetRestItems($collection, [], $this->startFirstPeriod, $this->hourLunch, 180, []);
        $restProcessedPeriod = $this->removeProcessedPeriod($collection, Arr::last($firstPeriod));

        $secondPeriod = $this->processedPeriodAndGetRestItems(collect($restProcessedPeriod), [], $this->startSecondPeriod, $this->happyHour, 240, []);
        $restProcessedPeriod = $this->removeProcessedPeriod(collect($restProcessedPeriod), Arr::last($secondPeriod));

        return [
            array_filter(
                array_merge(Arr::first($firstPeriod), Arr::first($secondPeriod))
            ),
            $restProcessedPeriod
        ];
    }

    private function processedPeriodAndGetRestItems(Collection $collection, array $durationPeriod, string $startTalk,
                                   string $endPeriod, int $limitPeriodInMinutes, array $removedItems)
    {
        $period = $collection->map(function ($valueItem, $key) use (
            &$durationPeriod, &$startTalk, $limitPeriodInMinutes, $endPeriod, &$removedItems
        ) {
            $totalMinutes = $valueItem['duration'] + array_sum($durationPeriod);
            if ($totalMinutes > $limitPeriodInMinutes) {
                return null;
            }

            $durationPeriod[] = $valueItem['duration'];
            $removedItems[] = $key;
            $fullTitle = $startTalk . ' ' . $valueItem['title'];
            $startTalk = $this->horary($startTalk, $valueItem['duration']);

            return $fullTitle;

        })->toArray();

        $period[] = !empty($period) ? $endPeriod : null;

        return [
            array_filter($period),
            $removedItems
        ];
    }

    private function getDataFile(): array
    {
        $filename = public_path('uploads') . '/eventos.txt';
        if(!File::exists($filename)) {
            return [];
        }

        $file = File::get($filename);
        return array_filter(array_map('trim', explode(',', $file)));
    }
}
