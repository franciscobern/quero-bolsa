<?php

namespace App\Http\Controllers\Forms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SendEventsFileController extends Controller
{
    public function create()
    {
        return view('send-events-file.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:txt|max:2048',
        ]);

        $fileName = 'eventos.'.$request->file->extension();

        $request->file->move(public_path('uploads'), $fileName);

        return back()
            ->with('success','Arquivo enviado com sucesso.')
            ->with('file', $fileName);
    }
}
